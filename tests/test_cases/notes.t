---
...

One line, empty header. | id:19bf0769d8470228f2d6ad7885b45f1a92fef7b8
---
title: Foo
keywords:
    - bar
    - baz
...

A
B
C | id:9a75aa89871f82b63f7d2a49e19cf5c5c8756859
---
keywords:
    - fingolfin
    - feanor
...
One line, full header, no separator. | id:64122c949e4961d26d0f747d161f1aa5dbbb5557
One line, no header. | id:a414b0c647a46887acc6d658725bf21ff5009673
---
keywords:
    - fingolfin
    - feanor
...

One line, full header, separator. | id:f6f2ffb4a618f3ca16462fd058fec872cec8c1e2
Trivial note at end. | id:e973078207652cd25a318f833b69b87d9c6cab43
