#!/usr/bin/env bats

load '/usr/lib/bats-support/load.bash'
load '/usr/lib/bats-assert/load.bash'


setup() {
    source ../lib/note-utils.sh
}

@test "extract_text: note_simple" {
    run extract_text <test_cases/note_simple
    assert_output "Foo is bar."
}

@test "extract_text: note_no_header" {
    run extract_text <test_cases/note_no_header
    assert_output "Quux."
}

@test "extract_text: note_empty_header" {
    run extract_text <test_cases/note_empty_header
    assert_output "Quux is quuz."
}

@test "extract_tags: note_simple" {
    run extract_tags <test_cases/note_simple
    assert_output ""
}

@test "extract_tags: note_no_empty_line_after_header" {
    run extract_text <test_cases/note_no_empty_line_after_header
    assert_output "Bar is foo."
}

@test "extract_tags: note_multiline_text" {
    run extract_text <test_cases/note_multiline_text
    assert_output $'A\nB\nC'
}


@test "extract_tags: note_no_header" {
    run extract_tags <test_cases/note_no_header
    assert_output ""
}

@test "extract_tags: note_simple" {
    run extract_tags <test_cases/note_simple
    assert_output $'fingolfin\nfeanor'
}

@test "extract_tags: note_no_header" {
    run extract_tags <test_cases/note_no_header
    assert_output ""
}

@test "extract_tags: note_empty_header" {
    run extract_tags <test_cases/note_empty_header
    assert_output ""
}
