#!/usr/bin/env bats

load '/usr/lib/bats-support/load.bash'
load '/usr/lib/bats-assert/load.bash'

setup() {
    source ../lib/note-utils.sh
}

@test "wrap into section: note_simple" {
    run wrap_leading_text_into_section <test_cases/note_simple
    assert_output "$(cat <<EOF
---
keywords:
    - fingolfin
    - feanor
...

<section>

Foo is bar.

</section>
EOF
)"
}

@test "wrap into section: note_no_header" {
    run wrap_leading_text_into_section <test_cases/note_no_header
    assert_output $'<section>\n\nQuux.\n\n</section>'
}

@test "wrap into section: note_empty_header" {
    run wrap_leading_text_into_section <test_cases/note_empty_header
    assert_output $'<section>\n\nQuux is quuz.\n\n</section>'
}

@test "wrap into section: note_no_empty_line_after_header" {
    run wrap_leading_text_into_section <test_cases/note_no_empty_line_after_header
    assert_output "$(cat <<EOF
---
keywords:
    - fingolfin
    - feanor
...

<section>

Bar is foo.

</section>
EOF
)"
}

@test "wrap into section: note_multiline_text" {
    run wrap_leading_text_into_section <test_cases/note_multiline_text
    assert_output "$(cat <<EOF
---
title: Foo
keywords:
    - bar
    - baz
...

<section>

A
B
C

</section>
EOF
)"
}

@test "wrap into section: note_with_header" {
    run wrap_leading_text_into_section <test_cases/note_with_header
    assert_output "$(cat <<EOF
---
title: Foo
keywords:
    - bar
    - baz
...

<section>

A

</section>

# Header

B
C
EOF
)"
}

@test "wrap into section: note_with_multiple_headers" {
    run wrap_leading_text_into_section <test_cases/note_with_multiple_headers
    assert_output "$(cat <<EOF
---
title: Foo
keywords:
    - bar
    - baz
...

<section>

A
B

</section>

# Header1

C
D

# Header2

E
F
EOF
)"
}
