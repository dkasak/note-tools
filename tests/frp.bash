awk_() {
    local awk_script
    awk_script="$(cat)"

    awk "$@" -f <(printf '%s\n' "$awk_script") < <(seq 1 10)
    return $?
}

awk_frp() {
    awk_ "$@" -f "../lib/frp.awk" < <(cat)
}
