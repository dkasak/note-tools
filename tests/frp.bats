#!/usr/bin/env bats

load '/usr/lib/bats-support/load.bash'
load '/usr/lib/bats-assert/load.bash'

load "$BATS_TEST_DIRNAME/frp.bash"

@test "is(s) = ¬is_not(s)" {
    run awk_frp <<EOF
        is("s") != !is_not("s") {
            print "ERROR: is(s) != ¬is_not(s)"
            exit 1
        }

        NR == 3 {
            set("s")
        }

        NR == 6 {
            reset("s")
        }

        END {
            # If we exited normally (not through an explicit exit)...
            if (!getline) {
                print "OK"
                exit 0
            }
        }
EOF

    assert_success
}

@test "set(s) sets rising edge for s immediately" {
    run awk_frp <<EOF
        BEGIN {
            set("s")
        }

        now_true("s") {
            exit 0
        }

        { exit 1 }
EOF

    assert_success
}

@test "set(s) doesn't immediately produce a falling edge" {
    run awk_frp <<EOF
        BEGIN {
            set("s")
        }

        now_false("s") {
            exit 1
        }

        now_true("s") {
            exit 0
        }

        { exit 2 }
EOF

    assert_success
}

@test "set(s); reset(s) leaves state s reset" {
    run awk_frp <<EOF
        BEGIN {
            set("s")
            reset("s")
        }

        is("s") {
            print "WRONG"
            exit 1
        }

        is_not("s") {
            print "OK"
            exit 0
        }

        {
            print "WRONG"
            exit 2
        }
EOF

    assert_success
}

@test "reset(s); set(s) leaves state s set" {
    run awk_frp <<EOF
        BEGIN {
            reset("s")
            set("s")
        }

        is_not("s") {
            exit 1
        }

        is("s") {
            exit 0
        }

        { exit 2 }
EOF

    assert_success
}

@test "When s is set, reset_in(s, p) followed by set_in(s, q) is valid if p < q" {
    run awk_frp <<EOF
        NR == 1 {
            set("s")
            reset_in("s", 3)
            set_in("s", 5)
        }

        NR == 1 && is_not("s") {
            exit 1
        }

        NR == 4 && !(is_not("s") && now_false("s")) {
            exit 2
        }

        NR == 6 && !(is("s") && now_true("s")) {
            exit 3
        }

        END {
            # If we exited normally (not through an explicit exit)...
            if (!getline) {
                print "OK"
                exit 0
            }
        }
EOF

    assert_success
}

@test "When s is reset, set_in(s, p) followed by reset_in(s, q) is valid if p < q" {
    run awk_frp <<EOF
        NR == 1 {
            reset("s")
            set_in("s", 3)
            reset_in("s", 5)
        }

        NR == 1 && !(is_not("s")) {
            exit 1
        }

        NR == 4 && !(is("s") && now_true("s")) {
            exit 2
        }

        NR == 6 && !(is_not("s") && now_false("s")) {
            exit 3
        }

        END {
            # If we exited normally (not through an explicit exit)...
            if (!getline) {
                print "OK"
                exit 0
            }
        }
EOF

    assert_success
}

@test "set(s) doesn't produce a rising edge for an already set state" {
    run awk_frp <<EOF
        NR == 1 {
            set("s")
        }

        NR == 2 {
            set("s")
        }

        NR == 2 && now_true("s") {
            print "ERROR: set(s) produced a rising edge for s, but it was already set."
            exit 1
        }

        END {
            # If we exited normally (not through an explicit exit)...
            if (!getline) {
                print "OK"
                exit 0
            }
        }
EOF

    assert_success
}

@test "reset(s) doesn't produce a falling edge for an reset state" {
    run awk_frp <<EOF
        NR == 1 {
            reset("s")
        }

        NR == 1 && now_false("s") {
            print "ERROR: reset(s) produced a falling edge for s, but it was reset."
            exit 1
        }

        END {
            # If we exited normally (not through an explicit exit)...
            if (!getline) {
                print "OK"
                exit 0
            }
        }
EOF

    assert_success
}

@test "set(s); reset(s) sets both edges" {
    run awk_frp <<EOF
        BEGIN {
            set("s")
            reset("s")
        }

        !now_true("s") {
            exit 1
        }

        !now_false("s") {
            exit 2
        }

        { exit 0 }
EOF

    assert_success
}

@test "set_in(s, n) on tick T has rising edge for s on tick T+n" {
    run awk_frp <<EOF
        NR == 1 {
            n = 3
            T = NR
            set_in("s", n)
        }

        is("s") && NR != T+n {
            print "ERROR: fired event on wrong tick."
            exit 1
        }

        is("s") && NR == T+n {
            print "OK"
            exit 0
        }

        END {
            # If we exited normally (not through an explicit exit)...
            if (!getline) {
                print "ERROR: Never fired the event."
                exit 2
            }
        }
EOF

    assert_success
}

@test "set(s) in BEGIN equals set(s) on line 1" {
    run awk_frp <<EOF
        BEGIN {
            set("s")
        }

        NR == 1 {
            set("r")
        }

        now_true("s") {
            line_s = NR
        }

        now_true("r") {
            line_r = NR

            if (line_s == line_r) {
                exit 0
            } else {
                exit 1
            }

        }

        END {
            # If we exited normally (not through an explicit exit)...
            if (!getline) {
                print "ERROR: Never fired any event."
                exit 2
            }
        }
EOF

    assert_success
}

@test "reset(s) erases scheduled (i.e. reset_in) future reset to ensure there is no falling edge while reset" {
    run awk_frp <<EOF
        NR == 1 {
            set("s")
            reset_in("s", 3)
            reset("s")
        }

        NR == 4 {
            set("s")
        }

        NR == 4 && is("s") && now_false("s") {
            print "ERROR: state s is set but has a falling edge"
            exit 1
        }

        END {
            # If we exited normally (not through an explicit exit)...
            if (!getline) {
                print "OK"
                exit 0
            }
        }
EOF

    assert_success
}

@test "set(s) erases scheduled (i.e. set_in) future set to ensure there is no rising edge while set" {
    run awk_frp <<EOF
        NR == 1 {
            reset("s")
            set_in("s", 3)
            set("s")
        }

        NR == 4 {
            reset("s")
        }

        NR == 4 && is_not("s") && now_true("s") {
            print "ERROR: state s is reset but has a rising edge"
            exit 1
        }

        END {
            # If we exited normally (not through an explicit exit)...
            if (!getline) {
                print "OK"
                exit 0
            }
        }
EOF

    assert_success
}

@test "A rising edge for a state cannot happen on a tick it wasn't set on" {
    run awk_frp <<EOF
        NR == 1 {
            set("s")
            reset_in("s", 3)
            set_in("s", 5)
            reset("s")
            set("s")

        }

        NR == 6 && is("s") && now_true("s") {
            print "ERROR: state s has a rising edge on a tick it wasn't set on"
            exit 1
        }

        END {
            # If we exited normally (not through an explicit exit)...
            if (!getline) {
                print "OK"
                exit 0
            }
        }
EOF

    assert_success
}

@test "A falling edge for a state cannot happen on a tick it wasn't reset on" {
    run awk_frp <<EOF
        NR == 1 {
            reset("s")
            set_in("s", 3)
            reset_in("s", 5)
            set("s")
            reset("s")

        }

        NR == 6 && is_not("s") && now_false("s") {
            print "ERROR: state s has a falling edge on a tick it wasn't reset on"
            exit 1
        }

        END {
            # If we exited normally (not through an explicit exit)...
            if (!getline) {
                print "OK"
                exit 0
            }
        }
EOF

    assert_success
}

@test "fire(s) equals set(s); reset_in(s, 1)" {
    run awk_frp <<EOF
        BEGIN {
            n = 1
        }

        NR == n {
            fire("s")
            set("r")
            reset_in("r", 1)
        }

        NR == n &&
        !(is("s") && is("r") &&
          now_true("s") && now_true("r") &&
          !now_false("s") && !now_false("r")) {

            print "ERROR: set(s) equivalency compromised."
            exit 1
        }

        NR == n+1 &&
        !(is_not("s") && is_not("r") &&
          now_false("s") && now_false("r") &&
          !now_true("s") && !now_true("r")) {

            print "ERROR: reset(s) equivalency compromised."
            exit 1
        }

        END {
            # If we exited normally (not through an explicit exit)...
            if (!getline) {
                print "OK"
                exit 0
            }
        }
EOF

    assert_success
}

@test "fire_in(s, t) equals set_in(s, t); reset_in(s, t+1)" {
    run awk_frp <<EOF
        BEGIN {
            n = 1
            t = 3
        }

        NR == n {
            fire_in("s", t)
            set_in("r", t)
            reset_in("r", t+1)
        }

        NR == n+t &&
        !(is("s") && is("r") &&
          now_true("s") && now_true("r") &&
          !now_false("s") && !now_false("r")) {

            print "ERROR: set(s) equivalency compromised."
            exit 1
        }

        NR == n+t+1 &&
        !(is_not("s") && is_not("r") &&
          now_false("s") && now_false("r") &&
          !now_true("s") && !now_true("r")) {

            print "ERROR: reset(s) equivalency compromised."
            exit 1
        }

        END {
            # If we exited normally (not through an explicit exit)...
            if (!getline) {
                print "OK"
                exit 0
            }
        }
EOF

    assert_success
}

@test "fire(s) sets state s and produces rising edge on T, resets and produces falling edge on T+1" {
    run awk_frp <<EOF
        BEGIN {
            n = 1
        }

        NR == n {
            fire("s")

            if (is_not("s")) {
                print "ERROR: State s not set."
                exit 1
            } else if (!now_true("s")) {
                print "ERROR: No rising edge for s."
                exit 2
            }
        }

        NR == n+1 {
            if (is("s")) {
                print "ERROR: State s is set when it should have already been reset."
                exit 3
            } else if (!now_false("s")) {
                print "ERROR: No falling edge for s."
                exit 4
            } else {
                exit 0
            }
        }

        END {
            # If we exited normally (not through an explicit exit)...
            if (!getline) {
                print "ERROR: Something went terribly wrong."
                exit 5
            }
        }
EOF

    assert_success
}

@test "clear(s) clears the state s immediately, without a falling edge" {
    run awk_frp <<EOF
        BEGIN {
            set("s")
        }

        is("s") {
            clear("s")
            line = NR
        }

        NR == line && is_not("s") && !now_false("s") {
            print "OK"
            exit 0
        }

        END {
            # If we exited normally (not through an explicit exit)...
            if (!getline) {
                print "ERROR: clear(s) didn't clear state s or it produced a falling edge."
                exit 1
            }
        }
EOF

    assert_success
}

@test "If s was never set, was(s) does not hold" {
    run awk_frp <<EOF
        was("s") {
            print "ERROR: was(s) holds but s was never set"
            exit 1
        }

        END {
            print "OK"
            exit 0
        }
EOF

    assert_success
}

@test "was(s) holds after set(s); reset(s)" {
    run awk_frp <<EOF
        BEGIN {
            set("s")
            reset("s")
        }

        was("s") {
            print "OK"
            exit 0
        }

        !was("s") {
            print "ERROR: State s was set at one point but was(s) does not hold"
            exit 1
        }
EOF

    assert_success
}

@test "clear(s) resets was(s), but reset(s) does not" {
    run awk_frp <<EOF
        BEGIN {
            set("s")
            reset("s")
        }

        !was("s") {
            print "ERROR: reset(s) also made was(s) untrue"
            exit 1
        }

        {
            clear("s")
        }

        was("s") {
            print "ERROR: clear(s) did not make was(s) untrue"
            exit 2
        }

        {
            print "OK"
            exit 0
        }
EOF

    assert_success
}

@test "ever(s) holds after set_in(s, N)" {
    run awk_frp <<EOF
        BEGIN {
            set_in("s", 5)
        }

        ever("s") {
            print "OK"
            exit 0
        }

        !ever("s") {
            print "ERROR: set(s) but not ever(s)"
            exit 1
        }
EOF

    assert_success
}
