#!/usr/bin/env bats

load '/usr/lib/bats-support/load.bash'
load '/usr/lib/bats-assert/load.bash'

setup() {
    source ../lib/note-utils.sh
}

@test "extract_text: note_simple" {
    run extract_text <test_cases/notes.t
    assert_line "One line, empty header."
    assert_line "One line, full header, no separator."
    assert_line "One line, full header, separator."
    assert_line "One line, no header."
    assert_line 'A'
    assert_line 'B'
    assert_line 'C'
    assert_line "Trivial note at end."
}

@test "extract_tags: note_simple" {
    run extract_tags <test_cases/notes.t
    assert_line 'fingolfin'
    assert_line 'feanor'
    assert_line 'bar'
    assert_line 'baz'
}
