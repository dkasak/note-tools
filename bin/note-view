#!/usr/bin/bash

preamble() {
    if [[ -z "$XDG_DATA_HOME" ]]; then
        XDG_DATA_HOME="$HOME/.local/share/"
    fi
    note_tools_config_dir="$XDG_CONFIG_HOME/note-tools"
    note_tools_data_dir="$XDG_DATA_HOME/note-tools"
    source "$note_tools_data_dir/note-utils.sh"
    source "$note_tools_config_dir/config"
}

preamble

if [ -z "$1" ]; then
    echo "Usage: $(basename "$0") [--html] <note-number>"
    exit 1
fi

while [[ "$1" == -* ]]; do
    case "$1" in
        --html)
            html=1
            ;;
        --pdf)
            pdf=1
            ;;
    esac

    shift
done

partial_note_id="$1"
full_note_id="$(note_id_from_partial "$partial_note_id")"

if [ -n "$full_note_id" ]; then
    if [ -n "$html" ]; then
        rendered_file="$(note-render --temp-file --fancy "$full_note_id" html)"
        _browser "$rendered_file"
        # rm "$rendered_file"
    elif [ -n "$pdf" ]; then
        rendered_file="$(note-render --temp-file --fancy "$full_note_id" pdf)"
        zathura "$rendered_file"
        rm "$rendered_file"
    else
        note_file="$(note-instantiate "$full_note_id")"
        note_content="$(cat "$note_file")"
        printf '||| Viewing note %s |||\n||| Changes will *not* be saved! |||\n\n%s' \
            "$full_note_id" "$note_content" > "$note_file"
        spawn_editor "$note_file"
        rm "$note_file"
    fi
else
    notify-send note-tools "[!] Note with ID $partial_note_id not found."
fi
