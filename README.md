# Introduction

These are my personal note-taking tools. They are similar in spirit to
Luhmann's Zettelkasten, but I only found out about this system afterwards.

This is currently intended only for my personal use and is therefore very
alpha-level software. Use at your own risk.

# Installation

    make install

*Be aware* that this installs into your home directory under `$HOME/.local/bin`
and `$HOME/.local/share/note-tools`. Luckily, there's also an uninstall target:

    make uninstall

# Planned rewrite

This should eventually be rewritten in a sane language to stop relying on
external tools as much. This especially means [t](https://github.com/sjl/t),
since it's not really ideal for this task.
