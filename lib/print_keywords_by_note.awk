function print_note_keywords() {
    keywords = ""

    for (i in note_keywords)
        if (keywords)
            keywords = keywords "," note_keywords[i]
        else
            keywords = note_keywords[i]

    return keywords
}

now_true("last_line") {
    printf "%s:%s\n",note_id,print_note_keywords()
}
