now_true("in_content") && is_not("on_markdown_header") {
    note_text = "<section>\n\n" note_text
    opened_section = 1
}

now_true("on_first_markdown_header") && opened_section {
    sub("\n[^\n]+$", "\n</section>\n&", note_text)
    opened_section = 0
}

now_true("last_line") && opened_section {
    sub("\n[^\n]+$", "\n</section>\n&", note_text)
    opened_section = 0
}

now_true("last_line") {
    opened_section = 0
}

END {
    if (opened_section) {
        opened_section = 0
        sub("\n[^\n]+$", "&\n\n</section>", note_text)
    }
}
