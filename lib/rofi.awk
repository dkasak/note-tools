function print_note_as_rofi() {
    gsub(/\n+/, " ", note_text)

    note_id_suffix = note_id ? " | " note_id : ""

    keywords = ""
    for (i in note_keywords) {
        keywords = keywords ?
                   keywords " #" note_keywords[i] :
                   "#" note_keywords[i]
    }

    if (note_topic) topic = "+" note_topic
    else topic = ""

    tags = ""

    if (topic) tags = tags ? tags " " topic : topic

    if (note_date) {
        date_str = note_date ? "date:" note_date : ""
        tags = tags ? tags " " date_str : date_str
    }

    if (note_content_date) {
        content_date_str = note_content_date ? "content-date:" note_content_date : ""
        tags = tags ? tags " " content_date_str : content_date_str
    }

    if (keywords) tags = tags ? tags " " keywords : keywords
    if (note_content_author) tags = tags ? tags " ~" note_content_author "~" : " ~" note_content_author "~"
    if (note_author) tags = tags ? tags " %" note_author "%" : " %" note_author "%"

    note_misc_headers_str = ""
    if (length(note_misc_headers) > 0) {
        for (header in note_misc_headers) {
            note_misc_headers_str = header ":" note_misc_headers[header]
        }
    }

    if (note_title == "")
        note_title_sep = ""
    else
        note_title_sep = ": "

    print (topic == "" ? "" : "|" note_topic "| ") \
          note_title \
          (note_subtitle == "" ? "" : " (" note_subtitle ")") \
          note_title_sep \
          note_text, "|",
          tags, note_misc_headers_str note_id_suffix
}

now_true("last_line") {
    print_note_as_rofi()
}

END {
    if (now_true("last_line") && !note_id)
        print_note_as_rofi()
}
