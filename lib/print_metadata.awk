function print_note_metadata() {
    if (note_metadata) {
        print "---"
        print note_metadata
        printf "...\n\n"
    }
}

now_true("last_line") {
    print_note_metadata()
}

END {
    if (!note_id) {
        print_note_metadata()
    }
}
