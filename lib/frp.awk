# Discrete FRP-ish model of states ("behaviours") and accompanying events.
#
# A state is designated by its name. Each state implicitly has two events
# related to it: one denoting the rising and one the falling edge of the state.
# Each event lasts for one time tick (the one immediately after the
# rising/falling edge). The names of the events are obtained by prefixing the
# state name by "start_" and "end_", respectively.
#
# Each state (and accompanying events) implicitly relates to a time `t`, which
# is implicitly encoded in the state. States therefore need to be manually
# stepped using the `step` function. This time is shown as `{t}` in signatures.

# Ensure entry exists for state s in the state table
function _ensure_state(s) {
    if (!(s in state))
        state[s] = 0
}

# Step time by one tick.
function _step() {
    for (s in state) {
        _step_state(s)
    }
}

# Advance time for the state s.
#
# Also sets or resets start/end events related to s, if needed.
#
# State -> Unit
function _step_state(s) {
    if (event["start_" s]) {
        --event["start_" s]
    }

    if (event["start_" s] == 1) {
        state[s] = 1
        state["_was_" s] = 1
    }

    if (event["end_" s]) {
        --event["end_" s]
    }

    if (event["end_" s] == 1) {
        state[s] = 0
    }
}

# Is the state currently active?
#
# State -> Bool
function is(s) {
    _ensure_state(s)
    return state[s] == 1
}

# Is the state currently inactive?
#
# State -> Bool
function is_not(s) {
    _ensure_state(s)
    return !is(s)
}

# Has the state become active during the current time tick (i.e. its
# start event is firing)?
#
# State -> Bool
function now_true(s) {
    return event["start_" s] == 1
}

# Has the state become inactive during the current time tick (i.e.
# its end event is firing)?
#
# State -> Bool
function now_false(s) {
    return event["end_" s] == 1
}

# Completely clear state (and all related data) immediately without firing any
# events.
#
# State -> Unit
function clear(s) {
    _ensure_state(s)
    event["end_" s] = 0
    state[s] = 0
    state["_was_" s] = 0
}

# Inactivate state immediately.
#
# State -> Unit
function reset(s) {
    _ensure_state(s)
    if (state[s] == 1) {
        event["end_" s] = 1
        state[s] = 0
    }
}

# Set state to become inactive in `t` time ticks.
#
# The state must either currently be active or set to become
# active.
#
# XXX: This could be improved to check whether it's set to become
# active *before* the reset would kick in.
#
# State -> Nat -> Unit
function reset_in(s, t) {
    _ensure_state(s)
    event["end_" s] = t + 1
}

# Activate state immediately.
#
# State -> Unit
function set(s) {
    _ensure_state(s)
    if (state[s] == 0) {
        event["start_" s] = 1
        state[s] = 1
        state["_was_" s] = 1
    }
}

# Set state to become active in `t` time ticks.
#
# The state must either currently be inactive or set to become
# inactive.
#
# XXX: This could be improved to check whether it's set to become inactive
# *before* the set would kick in. So, the check would look something like
# `not_before(s, t)`.
#
# State -> Nat -> Unit
function set_in(s, t) {
    _ensure_state(s)
    event["start_" s] = t + 1
}

# Is the state active currently or set to become active in the future?
#
# State -> Bool
function ever(s) {
    return state[s] == 1 || \
        (event["start_" s] > 1 && event["start_" s] != event["end_" s])
}

function fire(s) {
    set(s)
    reset_in(s, 1)
}

function fire_in(s, t) {
    set_in(s, t)
    reset_in(s, t + 1)
}

function was(s) {
    return is_not(s) && state["_was_" s]
}

{
    # Only step after the first real line.
    if (NR > 1) _step()
}
