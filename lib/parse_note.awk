# Parses either a single note directly or multiple notes as
# stored in t's internal format.
#
# IMPORTANT: the order of some rules matters!

function parse_yaml_attr_value() {
    result = $2
    for (i = 3; i <= NF; ++i)
        result = result FS $i

    return result
}

function push(arr, element) {
    arr[length(arr) + 1] = element
}

BEGIN {
    set("note")

    delete note_keywords[0]     # this creates the array
    delete note_misc_headers
}

debug {
    debug_line = $0
}

/.* \| id:[a-f0-9]{40}$/ {
    note_id = gensub(/.* \| id:([a-f0-9]{40})/, "\\1", 1, $0)
    reset_in("note", 1)
    fire("last_line")
}

END {
    reset("note")

    debug_prefix = debug_prefix " -note"
}

is_not("note") && /^[^\s]+/ {
    set("note")
}

now_true("note") {
    clear("in_keywords")
    clear("in_content")
    clear("metadata")
    clear("skipped_metadata")
    clear("seen_markdown_header")
    clear("on_markdown_header")
    clear("on_keywords_header")
    clear("on_first_markdown_header")

    note_id = ""
    note_title = ""
    note_subtitle = ""
    delete note_keywords
    note_date = ""
    note_topic = ""
    note_author = ""
    note_content_date = ""
    note_content_author = ""
    delete note_misc_headers
}

# Start of YAML header
is_not("metadata") && is_not("skipped_metadata") && /^---\s*$/ {
    set_in("metadata", 1)
}

# No YAML header:
# Upon reaching printable characters when we've never seen
# metadata nor we are currently inside metadata, conclude that
# there is in fact no metadata and that we are now in content.
!ever("metadata") && !/---\s*$/ && is_not("skipped_metadata") && /^\s*[[:print:]]/ {
    set("no_yaml_header")
    set("skipped_metadata")
    set("in_content")
}

# End of YAML header
is("metadata") && (/^\.\.\.\s*$/ || /^---\s*$/) {
    reset("in_keywords")
    reset("metadata")
    set_in("skipped_metadata", 1)
}

is("metadata") {
    if (match($0, /^keywords:/)) {
        fire("on_keywords_header")
    }
    else if (match($0, /^date:/)) {
        note_date = parse_yaml_attr_value()
    }
    else if (match($0, /^content-date:/)) {
        note_content_date = parse_yaml_attr_value()
    }
    else if (match($0, /^content-author:/)) {
        note_content_author = parse_yaml_attr_value()
    }
    else if (match($0, /^author:/)) {
        note_author = parse_yaml_attr_value()
    }
    else if (match($0, /^topic:/)) {
        note_topic = parse_yaml_attr_value()
    }
    else if (match($0, /^title:/)) {
        note_title = parse_yaml_attr_value()
    }
    else if (match($0, /^subtitle:/)) {
        note_subtitle = parse_yaml_attr_value()
    }
    else if (match($0, /([a-zA-Z-]+): ?(.+)$/, m) && is_not("last_line")) {
        note_misc_headers[m[1]] = m[2]
    }
}

now_true("on_keywords_header") {
    set_in("in_keywords", 1)
}

# Anything that doesn't look like a YAML list item while we
# are parsing keywords suggest that we have exited the
# keywords section.
is("metadata") && is("in_keywords") && !/^\s+- [[:alnum:]]+/ {
    reset("in_keywords")
}

is("in_keywords") {
    keyword = gensub(/^\s+- ([[:alnum:]]+)/, "\\1", "1", $0)

    push(note_keywords, keyword)
}

# Skip the empty line between YAML header and content, if it exists.
now_true("skipped_metadata") && is_not("in_content") {
    if (!NF) {
        set_in("in_content", 1)
        next
    } else {
        set("in_content")
    }
}

is("in_content") && /^# .+/ {
    if (is_not("seen_markdown_header")) {
        set("seen_markdown_header")
        set("on_first_markdown_header")
    }

    set("on_markdown_header")
}

!/^# .+/ {
    reset("on_markdown_header")
    reset("on_first_markdown_header")
}

debug {
    debug_prefix = "DEBUG: "

    if (now_false("note")) {
        debug_prefix = debug_prefix " -note"
    }

    if (now_true("note")) {
        debug_prefix = debug_prefix " +note"
    }

    if (now_true("in_content")) {
        debug_prefix = debug_prefix " +content"
    }

    if (now_true("in_keywords")) {
        debug_prefix = debug_prefix " +keywords"
    }

    if (now_false("in_keywords")) {
        debug_prefix = debug_prefix " -keywords"
    }

    if (now_true("metadata")) {
        debug_prefix = debug_prefix " +metadata"
    }

    if (now_false("metadata")) {
        debug_prefix = debug_prefix " -metadata"
    }

    if (now_true("on_keywords_header")) {
        debug_prefix = debug_prefix "  keywords_header"
    }

    if (is("in_keywords")) {
        debug_prefix = debug_prefix "  keyword"
    }

    if (is("metadata")) {
        debug_prefix = debug_prefix "  metadata"
    }

    if (is("on_markdown_header") && is_not("on_first_markdown_header")) {
        debug_prefix = debug_prefix "  header"
    }

    if (is("on_first_markdown_header")) {
        debug_prefix = debug_prefix "  !header"
    }

    if (is("in_content")) {
        debug_prefix = debug_prefix "  content"
    }

    if (now_true("last_line")) {
        debug_prefix = debug_prefix "  last_line"
    }

    if (now_true("no_yaml_header")) {
        debug_prefix = debug_prefix "  no_yaml_header"
    }

    printf "%-40s | %s\n", debug_prefix, $0
}
