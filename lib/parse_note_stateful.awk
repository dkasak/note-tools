function print_parsed_note() {
    print "DEBUG: # Note ID"
    print "DEBUG: " note_id

    print "DEBUG: # Note metadata"
    print "DEBUG: " note_metadata
    if (note_title)
        print "DEBUG: Note title:", note_title
    if (note_topic)
        print "DEBUG: Note topic:", note_topic
    if (note_date)
        print "DEBUG: Note date:", note_date
    if (note_content_date)
        print "DEBUG: Note content date:", note_content_date
    if (note_content_author)
        print "DEBUG: Note content author:", note_content_author

    print "DEBUG: # Note text"
    print "DEBUG: " note_text
    print "DEBUG: ================"
}

BEGIN {
    note_metadata = ""
    note_text = ""
}

now_true("note") {
    note_metadata = ""
    note_text = ""
}

is("metadata") && now_true("metadata") {
    note_metadata = $0
}

is("metadata") && !now_true("metadata") {
    note_metadata = note_metadata "\n" $0
}

is("in_content") && !now_true("last_line") {
    note_text = note_text ?
                note_text "\n" $0 :
                $0
}

now_true("last_line") {
    note_last_line = gensub(/(.*) \| id:[a-f0-9]{40}/, "\\1", 1, $0)

    if (is("in_content")) {
        note_text = note_text ?
                    note_text "\n" note_last_line :
                    note_last_line
    }
}

debug && now_true("last_line") {
    print_parsed_note()
}

END {
    if (!note_title) {
        if (!note_id) note_title = "Note " override_note_id
        else note_title = "Note " note_id
    }
}
