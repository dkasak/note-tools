preamble() {
    if [[ -z "$XDG_DATA_HOME" ]]; then
        XDG_DATA_HOME="$HOME/.local/share/"
    fi
    note_tools_config_dir="${XDG_CONFIG_HOME:-$HOME/.config}/note-tools"
    note_tools_data_dir="${XDG_DATA_HOME:-$HOME/.local/share}/note-tools"
    note_tools_cache_dir="${XDG_CACHE_HOME:-$HOME/.cache}/note-tools"
    note_tools_rofi_cache="$note_tools_cache_dir/cache"
    note_tools_last_note_file="$note_tools_cache_dir/last_note"
    source "$note_tools_config_dir/config"
}

preamble

note_metadata_regex='( \| id:[a-f0-9]{40}$)'
note_id_regex='id:[a-f0-9]{40}$'
url_regex='https?://[a-zA-Z0-9?/_.,=%#-]*'
old_tags_regex="[^:]+: "
link_regex=$'\\[\\[([^]]+)\\]\\]'

notify() {
    notify-send -a notemenu "$1" "$2"
}

# Runs a command in a new terminal
run_in_term() {
    "$TERMCMD" -e "$@" 2>/dev/null
}

# Spawn the EDITOR in a new terminal
spawn_editor() {
    local extra
    local editor_name="$(basename "$EDITOR")"
    if [[ "$editor_name" == "vim" ]] || \
       [[ "$editor_name" == "nvim" ]]; then
        extra+=('+silent /^\.\.\./')
        extra+=('+nohl')
        extra+=('+normal k')
    fi

    if [[ -n "$extra" ]]; then
        run_in_term "$EDITOR" "${extra[@]}" "$@"
    else
        run_in_term "$EDITOR" "$@"
    fi
}

last_note() {
    if [[ -f "$note_tools_last_note_file" ]]; then
        cat "$note_tools_last_note_file"
        return 0
    else
        return 1
    fi
}

set_last_note() {
    note_id="$1"
    printf '%s' "$note_id" > "$note_tools_last_note_file"
}

temp_file() {
    [ -n "$1" ] && optional_ext=".$1"
    mktemp -t notemenu.XXXXXXXXXX"${optional_ext}"
}

note_id_from_partial() {
    local partial_id="$1"

    # If given ID starts with zk: or zk://, just strip it
    partial_id=${partial_id#zk:}
    partial_id=${partial_id#parma:}
    partial_id=${partial_id#//}

    sed -ne "s/.*id:\(${partial_id}[a-zA-Z0-9]*\)$/\1/p" "$notes_path" \
      | tail -n 1
}

extract_text_old() {
    escape_backslashes      \
        | escape_newlines   \
        | cut -d: -f2-      \
        | sed -E 's/^\s*//' \
        | unescape_newlines \
        | unescape_backslashes
}

extract_tags_old() {
    head -n1 \
        | grep -Eo "$old_tags_regex" \
        | head -n1 \
        | tr -d :
}

extract_tags() {
    local DEBUG=0

    while [[ "$1" == -* ]]; do
        case "$1" in
            -d|--debug)
                DEBUG=1
                ;;
        esac
        shift
    done

    awk -v debug="$DEBUG" \
        -f "$note_tools_data_dir/frp.awk" \
        -f "$note_tools_data_dir/parse_note.awk" \
        -f "$note_tools_data_dir/print_keywords.awk"
}


extract_tags_by_note() {
    local DEBUG=0

    while [[ "$1" == -* ]]; do
        case "$1" in
            -d|--debug)
                DEBUG=1
                ;;
        esac
        shift
    done

    awk -v debug="$DEBUG" \
        -f "$note_tools_data_dir/frp.awk" \
        -f "$note_tools_data_dir/parse_note.awk" \
        -f "$note_tools_data_dir/print_keywords_by_note.awk"
}

# Given the note's content (metadata + text), separates out just the text.
#
# In essence, this strips out the YAML header, if it exists.
extract_text() {
    local DEBUG=0

    while [[ "$1" == -* ]]; do
        case "$1" in
            -d|--debug)
                DEBUG=1
                ;;
        esac
        shift
    done

    awk -v debug="$DEBUG" \
        -f "$note_tools_data_dir/frp.awk" \
        -f "$note_tools_data_dir/parse_note.awk" \
        -f "$note_tools_data_dir/parse_note_stateful.awk" \
        -f "$note_tools_data_dir/print_text.awk"
}

extract_urls() {
    rg -o "$url_regex"
}

wrap_leading_text_into_section() {
    # This is needed because Tufte CSS expects everything to be wrapped up into
    # sections. Unfortunately, pandoc doesn't wrap text before the first header
    # in section tags, so we have to do it manually.

    local DEBUG=0

    while [[ "$1" == -* ]]; do
        case "$1" in
            -d|--debug)
                DEBUG=1
                ;;
        esac
        shift
    done

    awk -v debug="$DEBUG" \
        -f "$note_tools_data_dir/frp.awk" \
        -f "$note_tools_data_dir/parse_note.awk" \
        -f "$note_tools_data_dir/parse_note_stateful.awk" \
        -f "$note_tools_data_dir/wrap_leading_text_into_section.awk" \
        -f "$note_tools_data_dir/print_metadata.awk" \
        -f "$note_tools_data_dir/print_text.awk"
}

pandoc_preprocess_for_fancy() {
    # Preprocess note into a form suitable for passing to pandoc when
    # converting to PDF or HTML and formatting with Tufte Pandoc CSS.

    local DEBUG=0
    local note_id

    while [[ "$1" == -* ]]; do
        case "$1" in
            -d|--debug)
                DEBUG=1
                ;;
            --id)
                note_id="$2"
                shift
                ;;
        esac
        shift
    done

    local note_text
    note_text="$(
        awk -v debug="$DEBUG" -v override_note_id="$note_id" \
            -f "$note_tools_data_dir/frp.awk" \
            -f "$note_tools_data_dir/pandoc_fix_note_links.awk" \
            -f "$note_tools_data_dir/parse_note.awk" \
            -f "$note_tools_data_dir/increase_markdown_header_level.awk" \
            -f "$note_tools_data_dir/parse_note_stateful.awk" \
            -f "$note_tools_data_dir/wrap_leading_text_into_section.awk" \
            -f "$note_tools_data_dir/print_metadata.awk" \
            -f "$note_tools_data_dir/print_text.awk")"

    note_text="$(resolve_title_links "$note_text")"

    printf '%s' "$note_text"
}

pandoc_preprocess_for_markdown() {
    # Preprocess note into a form suitable for passing to pandoc when
    # converting to plain Markdown (from the extended Pandoc Markdown syntax).

    local DEBUG=0
    local note_id

    while [[ "$1" == -* ]]; do
        case "$1" in
            -d|--debug)
                DEBUG=1
                ;;
            --id)
                note_id="$2"
                shift
                ;;
        esac
        shift
    done

    local note_text
    note_text="$(
        awk -v debug="$DEBUG" -v override_note_id="$note_id" \
            -f "$note_tools_data_dir/frp.awk" \
            -f "$note_tools_data_dir/parse_note.awk" \
            -f "$note_tools_data_dir/increase_markdown_header_level.awk" \
            -f "$note_tools_data_dir/parse_note_stateful.awk" \
            -f "$note_tools_data_dir/add_title_as_header.awk" \
            -f "$note_tools_data_dir/print_text.awk")"

    note_text="$(resolve_title_links "$note_text")"

    printf '%s' "$note_text"
}

resolve_title_links() {
    local note_text="$1"

    local title
    while read -r link; do
        # Don't try using rg instead of tr, see https://github.com/BurntSushi/ripgrep/issues/1311
        title="$(printf "$link" \
                | tr -d '\n' \
                | rg --multiline --passthru --replace ' ' '\s+' \
                | rg --multiline --passthru --replace '$1' "$link_regex" \
                | tr -d '"')"
        escaped_link="$(printf '%s' "$link" \
                       | tr -d '"' \
                       | rg --passthru '\+' --replace '\+'       \
                       | rg --passthru '\[\[' --replace '\[\['   \
                       | rg --passthru '\]\]' --replace '\]\]')"

        link_note_id="$(note-search -i "^title: $title" | head -n 1)"

        # If the link is pointing to a note, replace the link with the note's ID
        if [[ -n "$link_note_id" ]]; then
            note_text="$(printf '%s' "$note_text" | \
                         rg --multiline --passthru --replace "[$title](zk:$link_note_id)" "$escaped_link")"
        fi
    done < <(printf '%s' "$note_text" \
            | rg --multiline -o --json "$link_regex" \
            | jq -r 'select(.type == "match") | .data.submatches | .[].match.text | tojson')

    printf '%s' "$note_text"
}

# Remove newlines from notes so they are one long line. This is to enable
# full-text search from rofi.
notes_to_rofi() {
    local DEBUG=0

    while [[ "$1" == -* ]]; do
        case "$1" in
            -d|--debug)
                DEBUG=1
                ;;
        esac
        shift
    done

    awk -v debug="$DEBUG" \
        -f "$note_tools_data_dir/frp.awk" \
        -f "$note_tools_data_dir/parse_note.awk" \
        -f "$note_tools_data_dir/parse_note_stateful.awk" \
        -f "$note_tools_data_dir/rofi.awk"
}

tags_to_json() {
    while read -r tag; do
        printf '"%s"\n' "$tag"
    done | paste -sd "," -
}

text_to_json() {
    escape_backslashes | escape_newlines | escape_quotes
}

json_to_text() {
    unescape_quotes | unescape_newlines | unescape_backslashes
}

escape_backslashes() {
    sed 's/\\/\\\\/g'
}

unescape_backslashes() {
    sed 's/\\\\/\\/g'
}

escape_newlines() {
    # https://stackoverflow.com/a/1252191/5276520
    sed ':a;N;$!ba;s/\n/\\n/g'
}

unescape_newlines() {
    sed -r 's/([^\\]|\\\\)\\n/\1\n/g' | sed -r 's/^\\n/\n/'
}

escape_quotes() {
    awk '{ gsub("\"", "\\\"", $0); print $0 }'
}

unescape_quotes() {
    awk '{ gsub("\\\"", "\"", $0); print $0 }'
}

# Trim leading and trailing whitespace.
trim() {
    local text="$1"

    # trim leading
    text="${text#"${text%%[![:space:]]*}"}"

    # trim trailing
    text="${text%"${text##*[![:space:]]}"}"

    printf '%s' "$text"
}

# Join elements in array using a single char
join_by() {
    IFS="$1"
    shift
    echo "$*"
}

author() {
    printf '%s\n' "$note_author"
}

metadata_header() {
    title="$1"
    tags="$2"

    if [[ -n "$title" ]]; then
        cat <<EOF
---
author: $(author)
title: $title
$(metadata_keywords "$tags")...
EOF

else

        cat <<EOF
---
author: $(author)
$(metadata_keywords "$tags")...
EOF

fi
}

metadata_keywords() {
    tags="$1"

    if [[ -z "$tags" ]]; then
        return
    fi

    printf 'keywords:\n'
    tr , '\n' <<<"$tags" | while read -r line; do
        printf '  - %s\n' "$line"
    done
}

construct_note_json() {
    note_tags="$(tags_to_json <<<"$1")"
    note_content="$(text_to_json <<<"$2")"

    json="""
{
    \"author\": \"$(author)\",
    \"tags\": [$note_tags],
    \"content\": \"$note_content\"
}
"""

    printf "%s\n" "$json"
}

old_to_new_format() {
    local note_text="$1"
    local note_tags="$2"

    # If the note is a single line and there is a URL trailing, put it on its
    # own line.
    local regex='^(.+)(<https?://[^>]*>)[[:space:]]*$'
    if [[ "$(wc -l <<<"$note_text")" -eq 1 ]] && [[ "$note_text" =~ $regex ]]; then
        note_text="$(printf '%s\n\n%s' "${BASH_REMATCH[1]}" "${BASH_REMATCH[2]}")"
    fi

    # Extract first line.
    first_line="$(head -n 1 <<<"$note_text")"
    first_line_len="$(wc -c <<<"$first_line")"

    # Save note text without the first line.
    note_text="$(tail -n+2 <<<"$note_text")"

    # Is the first line a title?
    if (( first_line_len < 65 )); then
        title="$(trim "$first_line")"
        note_text="$(trim "$note_text")"
        note_text="$(trim "$note_text")"
    fi

    metadata_header "$title" "$note_tags"
    printf '\n'

    if [[ -z "$title" ]]; then
        # If not a title, re-wrap the first line and add it back to the
        # note text. This is needed because I used to write the first line
        # unwrapped, due to limitations of the old system.

        note_text="$(cat <(fmt -w 80 <<<"$first_line") \
                         <(printf '%s' "$note_text"))"

    fi

    # Remove trailing spaces
    note_text="$(sed -r 's/\s+$//' <<<"$note_text")"

    printf '%s' "$note_text"
}

_browser() {
    local BROWSER="${BROWSER:-firefox}"

    if [[ "$BROWSER" == "firefox" ]]; then
        if [[ -n "$browser_profile" ]]; then
            "$BROWSER" -P "$browser_profile" --new-tab "$@"
        else
            "$BROWSER" --new-tab "$@"
        fi
    else
        "$BROWSER" "$@"
    fi
}

create_cache_dir() {
    local note_tools_rofi_cache_dir="$(dirname "$note_tools_rofi_cache")"

    if [[ ! -d "$note_tools_rofi_cache_dir" ]]; then
        mkdir -p "$note_tools_rofi_cache_dir"
    fi
}

ensure_rofi_cache_exists() {
    create_cache_dir

    if [[ ! -f "$note_tools_rofi_cache" ]]; then
        _generate_rofi_cache
    fi
}

update_rofi_cache() {
    create_cache_dir
    _generate_rofi_cache
}

_generate_rofi_cache() {
    tmp=$(mktemp)
    notes_to_rofi <"$notes_path" >"$tmp"
    mv "$tmp" "$note_tools_rofi_cache"
}

archive() {
    local url="$1"

    # Use archivenow if available, otherwise fall back to
    # manual archiving to the two most used service:
    # archive.is and the Internet Archive.
    if command -v archivenow >/dev/null; then
        archivenow --is --ia --st --mg "$url" >/dev/null & disown
    else
        curl --data url="$url" http://archive.is/submit/ >/dev/null  & disown
        curl "https://web.archive.org/save/$url" >/dev/null & disown
    fi
}
