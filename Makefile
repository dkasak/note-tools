.PHONY: install develop uninstall test

NAME := note-tools

PREFIX ?= $(HOME)/.local
DATA_HOME := $(PREFIX)/share

BIN_DIR  := $(DESTDIR)$(PREFIX)/bin
LIB_DIR  := $(DESTDIR)$(DATA_HOME)/$(NAME)
DATA_DIR := $(DESTDIR)$(DATA_HOME)/$(NAME)

# We don't care about the sort order, just the fact that sort has the
# side-effect of removing duplicates.
INSTALL_DIRS := $(sort $(BIN_DIR) $(LIB_DIR) $(DATA_DIR))

INSTALL         ?= install
INSTALL_PROGRAM ?= $(INSTALL)
INSTALL_DATA    ?= $(INSTALL) -D -m644

BINS := bin/note \
	bin/note-delete \
	bin/note-edit \
	bin/note-ids \
	bin/note-instantiate \
	bin/note-new \
	bin/note-plot \
	bin/note-render \
	bin/note-search \
	bin/note-share \
	bin/note-show \
	bin/note-tags \
	bin/note-urls \
	bin/note-view \
	bin/notemenu

LIBS := lib/frp.awk \
	lib/parse_note.awk \
	lib/parse_note_stateful.awk \
	lib/print_keywords_by_note.awk \
	lib/print_keywords.awk \
	lib/print_metadata.awk \
	lib/print_text.awk \
	lib/add_title_as_header.awk \
	lib/rofi.awk \
	lib/wrap_leading_text_into_section.awk \
	lib/pandoc_fix_note_links.awk \
	lib/increase_markdown_header_level.awk \
	lib/note-utils.sh

DATA_FILES := \
	style/tufte.css \
	style/tufte-extra.css \
	style/tufte.html5 \
	style/tufte-handout.tex \
	style/tufte-handout-custom.tex \
	style/latex.css \
	style/plain.css \
	style/pandoc.css \
	templates/default \
	templates/default-with-title \
	templates/diary

FONT := style/et-book

DST_BINS := $(addprefix $(BIN_DIR)/,$(notdir $(BINS)))
DST_LIBS := $(addprefix $(LIB_DIR)/,$(notdir $(LIBS)))
DST_DATA_FILES := $(addprefix $(DATA_DIR)/,$(DATA_FILES))
DST_FONT := $(addprefix $(DATA_DIR)/,$(notdir $(FONT)))

DST_ALL := $(DST_BINS) $(DST_LIBS) $(DST_DATA_FILES) $(DST_FONT)

install: $(INSTALL_DIRS) $(DST_ALL)

develop: INSTALL_PROGRAM := ln -s
develop: INSTALL_DATA := ln -s
develop: $(INSTALL_DIRS) $(DST_ALL)

uninstall:
	@for f in $(foreach f,$(DST_ALL),$f); do \
	    if [ -d $$f ]; then \
	        rm -r $$f ; \
	    elif [ \( -f $$f \) -o \( -L $$f \) ]; then \
	        rm $$f ; \
	    fi \
	done

test:
	cd tests && bats *.bats

$(DST_BINS) : ${BIN_DIR}/% : bin/%
	$(INSTALL_PROGRAM) $(abspath $<) $@

$(DST_LIBS) : ${LIB_DIR}/% : lib/%
	$(INSTALL_DATA) $(abspath $<) $@

$(DST_DATA_FILES) : ${DATA_DIR}/% : %
	$(INSTALL_DATA) $(abspath $<) $@

$(DST_FONT): INSTALL_DATA := cp -r
$(DST_FONT): ${DATA_DIR}/% : style/%
	$(INSTALL_DATA) $(abspath $<) $@

$(INSTALL_DIRS):
	$(INSTALL) -d $@
