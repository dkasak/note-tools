""" Insert some text at cursor position in the current buffer
function! s:InsertAtCursorPosition(text)
    let line = getline('.')
    call setline('.', strpart(line, 0, col('.') - 1) . a:text . strpart(line, col('.') - 1))
endfunction


""" Find matching pair of strings, such as matched brackets or syntactical
""" elements like if/endif.
"
" The arguments `left` and `right` are regexes.
"
" Returns a pair where the first element is the position of the opening
" character and the second element is the position of the closing character.
" Each position is itself a pair: [line_num, col_num] of the match.
function! s:FindPair(left, right)
    let save_cursor = getcurpos()
    let l:closing_char = searchpairpos(a:left, '', a:right, 'W')
    let l:opening_char = searchpairpos(a:left, '', a:right, 'bW')
    call setpos('.', save_cursor)

    return [l:opening_char, l:closing_char]
endfunction


""" Get text between a matching pair of strings, such as the one found by
""" FindPair.
"
" The arguments `left` and `right` are regexes that specify the pair. A third,
" optional argument, is the separator used to join the lines in case the text is
" spread over multiple lines. By default, the separator is ' '.
"
" Returns empty string if a matching pair cannot be found. Otherwise, returns
" the text between the pair.
function! s:TextBetween(left, right, ...)
    let l:sep = a:0 > 0 ? a:1 : ' '

    " Append \zs so that we match just *after* the starting string since we
    " don't need it and want to skip it for this purpose.
    let l:left = a:left . '\zs'

    let [l:opening_element, l:closing_element] = s:FindPair(l:left, a:right)

    " If either of the braces were not found, return early.
    if l:opening_element == [0, 0] || l:closing_element == [0, 0]
        return ""
    endif

    let l:first_line_num = l:opening_element[0]
    let l:last_line_num = l:closing_element[0]
    let l:first_line = getline(l:first_line_num)
    let l:last_line = getline(l:last_line_num)

    " Build a string with the text content.
    if l:first_line_num == l:last_line_num
        let l:text = l:first_line[l:opening_element[1] - 1:l:closing_element[1] - 2]
    else
        let l:text = l:first_line[l:opening_element[1] - 1:]

        let l:middle_start = l:first_line_num + 1
        let l:middle_end = l:last_line_num - 1

        if l:middle_end - l:middle_start >= -1
            for l:line_num in range(l:middle_start, l:middle_end)
                let l:text = l:text . l:sep . getline(l:line_num)
            endfor
        endif

        let l:text = l:text . l:sep . l:last_line[:l:closing_element[1] - 2]
    endif

    return l:text
endfunction

""" Convert inline Markdown link to a named one.
"
" Prompts the user for a link name and converts an inline Markdown link to
" a named one, placing it below the current paragraph.
"
" e.g.
"
"     [This](https://example.com) is a link.
"
" gets converted to
"
"     [This][USER_INPUT] is a link.
"
"     [USER_INPUT]: <https://example.com)
"
" Returns 1 on success, 0 on error.
"
" Recommended binding: _l
function! Markdown_InlineLinkToNamed()
    let l:link_name = input('Link name: ')

    " Return early if link name is empty (the user likely aborted).
    if empty(l:link_name)
        return 0
    endif

    let [l:opening_paren, l:closing_paren] = s:FindPair('(', ')')

    " If either of the parens were not found, return early.
    if l:opening_paren == [0, 0] || l:closing_paren == [0, 0]
        return 0
    endif

    let l:url = s:TextBetween('(', ')', '')

    " Delete until the closing paren.
    normal ldi)

    " Insert braces.
    normal h2cl[]

    " Insert link name.
    call s:InsertAtCursorPosition(l:link_name)

    " Format the paragraph.
    normal gwap

    " Skip to two lines below the current paragraph, adding empty lines if
    " necessary.
    normal }
    if line('.') == line('$')
        exe "normal o\ei\n\e"
    else
        exe "normal 2i\n\ek"
    endif

    exe "normal i[\el"
    call s:InsertAtCursorPosition(l:link_name)
    exe "normal $a]: \el"
    call s:InsertAtCursorPosition("<" . l:url . ">")

    " Format the link.
    normal gwap

    return 1
endfunction


""" Convert inline Markdown sidenote to a named one.
"
" Prompts the user for a sidenote name and converts an inline
" sidenote to a named one, placing the sidenote content below the current
" paragraph.
"
" e.g.
"
"     Foo is bar^[Except when not.].
"
" gets converted to
"
"     Foo is bar[^note].
"
"     [^note]: Except when not.
"
" Returns 1 on success, 0 on error.
"
" Recommended binding: _s
function! Markdown_InlineSidenoteToNamed()
    let l:sidenote_name = input('Sidenote name: ')

    " Return early if sidenote name is empty (the user likely aborted)
    if empty(l:sidenote_name)
        return 0
    endif

    let [l:opening_brace, l:closing_brace] = s:FindPair('\[', '\]')

    " If either of the braces were not found, return early.
    if l:opening_brace == [0, 0] || l:closing_brace == [0, 0]
        return 0
    endif

    let l:first_line_num = l:opening_brace[0]
    let l:last_line_num = l:closing_brace[0]
    let l:first_line = getline(l:first_line_num)
    let l:last_line = getline(l:last_line_num)

    " If the character before the opening brace is not a caret, this is not an
    " inline sidenote so return early.
    if l:first_line[l:opening_brace[1] - 2] !=# '^'
        return 0
    endif

    let l:sidenote = s:TextBetween('\[', '\]')

    " Delete inside the braces and move the caret inside the sidenote
    " reference.
    exe "normal ldi]2hxli^\el"

    " Insert sidenote name.
    call s:InsertAtCursorPosition(l:sidenote_name)

    " Format the paragraph from which the sidenote was extracted.
    normal gwap

    " Skip to two lines below the current paragraph, adding empty lines if
    " necessary.
    normal }
    if line('.') == line('$')
        exe "normal o\ei\n\e"
    else
        exe "normal 2i\n\ek"
    endif

    " Insert the new sidenote.
    exe "normal i[^\el"
    call s:InsertAtCursorPosition(l:sidenote_name)
    exe "normal $a]: \el"
    call s:InsertAtCursorPosition(l:sidenote)

    " Format the resulting sidenote.
    normal gwap

    return 1
endfunction


""" Open note, either by note title or via a note link.
"
" Given either a note name or a note link (zk:... or parma:...), opens the
" note using either `note-edit` (if `editable` is 1) or `note-view` (if
" `editable` is 0).
"
" If the note is open as editable and it was referenced via a title that does
" not exist, a new note will be created and opened in edit mode.
"
" Returns 0 if the note does not exist, 1 on success.
"
" Recommended binding: _o
function! Parma_OpenNote(note, editable)
    if stridx(a:note, "zk:") == 0 || stridx(a:note, "parma:") == 0
        let l:note = a:note[stridx(a:note, ":") + 1:]
        let l:title_note = 0
    else
        let l:note = trim(system("note-search -i " . shellescape("^title: " . a:note . "$")))
        let l:title_note = 1
    endif

    if len(l:note) != 40
        if a:editable && l:title_note
            " Note doesn't exist but we're creating a new one with the given
            " title.
            call jobstart(["note-new", "--title", a:note])
        else
            " Note doesn't exist and we won't be creating a new one.
            echoerr "Not a note ID nor a title of an existing note: " . a:note
            return 0
        endif
    else
        if a:editable
            call jobstart(["note-edit", l:note])
        else
            call jobstart(["note-view", l:note])
        endif
    endif

    return 1
endfunction

""" Extracts the target of a note link under cursor.
"
" The target can be either:
"
" 1. a note URI (zk:... or parma:...), in case of ordinary Markdown links
" 2. a note title, in case of [[...]] style of links
"
" Returns the target as a string if the cursor is on a note link. Otherwise
" returns the empty string.
function! Parma_getNoteLinkTarget()
    let l:target = s:TextBetween('<', '>')

    if empty(l:target)
        let l:target = s:TextBetween('(', ')')
    endif

    if empty(l:target)
        let l:target = s:TextBetween('\[\[', '\]\]')
    endif

    return l:target
endfunction


nnoremap          _l :call Markdown_InlineLinkToNamed()<CR>
nnoremap          _s :call Markdown_InlineSidenoteToNamed()<CR>
nnoremap <silent> _o :set isk+=:<CR>:<C-u>call Parma_OpenNote(Parma_getNoteLinkTarget(), 1)<CR>:set isk-=:<CR>
